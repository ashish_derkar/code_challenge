import { Controller, Get } from '@nestjs/common';

import { City } from '@angular-nest/api-interfaces';

import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('cities')
  getData(): City[] {
    return this.appService.getData();
  }
}
