import { Component, AfterViewInit, Input } from '@angular/core';
import * as L from 'leaflet';

const iconRetinaUrl =
  'https://unpkg.com/leaflet@1.6.0/dist/images/marker-icon.png';
const iconUrl = 'https://unpkg.com/leaflet@1.6.0/dist/images/marker-icon.png';
const shadowUrl =
  'https://unpkg.com/leaflet@1.6.0/dist/images/marker-shadow.png';
const iconDefault = L.icon({
  iconRetinaUrl,
  iconUrl,
  shadowUrl,
  iconSize: [25, 41],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  tooltipAnchor: [16, -28],
  shadowSize: [41, 41],
});
L.Marker.prototype.options.icon = iconDefault;
@Component({
  selector: 'angular-nest-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
})
export class MapComponent implements AfterViewInit {
  @Input() mapData: any;
  private map: any;
  latLong: any;
  private initMap(latLong: any): void {
    this.map = L.map('map', {
      center: latLong,
      zoom: 3,
    });

    const tiles = L.tileLayer(
      'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
      {
        maxZoom: 18,
        minZoom: 3,
        attribution:
          '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
      }
    );

    tiles.addTo(this.map);
  }

  ngAfterViewInit(): void {
    this.latLong = [+this.mapData.latitude, +this.mapData.longitude];
    this.initMap(this.latLong);
    L.marker(this.latLong)
      .bindPopup(
        '<b>' +
        this.mapData.name +
        '</b> <br>(' +
        this.mapData.latitude +
        ' , ' +
        this.mapData.longitude +
        ')'
      )
      .addTo(this.map);
  }
}
