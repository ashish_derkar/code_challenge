import { createFeatureSelector, createSelector } from '@ngrx/store';
import { DATA_FEATURE_KEY, State } from './data.reducer';

// Lookup the 'Data' feature state managed by NgRx
export const getDataState = createFeatureSelector<State>(DATA_FEATURE_KEY);


export const getDataLoaded = createSelector(
  getDataState,
  (state: State) => state.loaded
);

export const getDataError = createSelector(
  getDataState,
  (state: State) => state.error
);

export const getAllData = createSelector(
  getDataState, 
  (state: State) =>  state.cities
);

