import { createAction, props } from '@ngrx/store';

import { City } from '@angular-nest/api-interfaces';

export const init = createAction('[Data Page] Init');

export const loadDataSuccess = createAction(
  '[Data/API] Load Data Success',
  props<{ data: City[] }>()
);

export const loadDataFailure = createAction(
  '[Data/API] Load Data Failure',
  props<{ error: any }>()
);
