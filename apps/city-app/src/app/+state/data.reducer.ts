import { City } from '@angular-nest/api-interfaces';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { createReducer, on, Action } from '@ngrx/store';
import * as DataActions from './data.actions';


export const DATA_FEATURE_KEY = 'data';

export interface State extends EntityState<City> {
  cities: City[]; // which Data record has been selected
  loaded: boolean; // has the Data list been loaded
  error?: string | null; // last known error (if any)
}



export const dataAdapter: EntityAdapter<City> =
  createEntityAdapter<City>();

export const initialState: State = dataAdapter.getInitialState({
  // set initial required properties
  cities: [], // which Data record has been selected
  loaded: false
});

const dataReducer = createReducer(
  initialState,
  on(DataActions.init, (state) => ({ ...state, loaded: false, error: null })),
  on(DataActions.loadDataSuccess, (state, { data }) =>
    ({ ...state, cities: data, loaded: true })
  ),
  on(DataActions.loadDataFailure, (state, { error }) => ({ ...state, loaded: false, error: error }))
);

export function reducer(state: State | undefined, action: Action) {
  return dataReducer(state, action);
}
