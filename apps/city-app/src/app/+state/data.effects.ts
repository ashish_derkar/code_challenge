import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import * as DataActions from './data.actions';
import { map, exhaustMap, catchError, take } from 'rxjs/operators';
import { of } from 'rxjs';
import { CityService } from '../city.service';

@Injectable()
export class DataEffects {
  
  constructor(private readonly actions$: Actions, private cityService: CityService) {}

  init$ = createEffect(() =>
  this.actions$.pipe(
      ofType(DataActions.init),
      exhaustMap(() =>
        this.cityService.getCities().pipe(
          map(response => DataActions.loadDataSuccess({ data: response })),
          catchError((error: any) => of(DataActions.loadDataFailure(error))))
      ),
      take(1)
    )

    
  );

}
