import { City } from '@angular-nest/api-interfaces';
import { Component, OnInit } from '@angular/core';
import { CityService } from '../app/city.service';
import { MatDialog } from "@angular/material/dialog";
import { CityComponent } from './city/city.component';

import { Store, select } from '@ngrx/store';
import * as DataActions from '../app/+state/data.actions';
import * as selectData from '../app/+state/data.selectors';

@Component({
  selector: 'angular-nest-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  cities: City[] = [];
  errorMessage: string | null | undefined;
  
  constructor(private cityService: CityService, private matDialog: MatDialog, private store: Store) { }
  ngOnInit() {

    this.store.dispatch(DataActions.init()); // action dispatch

    this.store.pipe(select(selectData.getAllData)).subscribe(
      data => {
        this.cities = data;
        console.log("Get cities", this.cities);
      }
    )

    this.store.pipe(select(selectData.getDataError)).subscribe(
      err => {
        console.log("Get err", err);
        this.errorMessage = err;

      }
    )

  }

  OpenModal(city: any) {
    this.matDialog.open(CityComponent, {
      data: city,
      disableClose: true,
      width: '600px'
    });
  }
}
