import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '@angular-nest/material';
import { HeaderComponent } from './header/header.component';
import { CityComponent } from './city/city.component';
import { MapComponent } from './map/map.component';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import * as fromData from './+state/data.reducer';
import { DataEffects } from './+state/data.effects';
import { environment } from '../environments/environment';
import { StoreDevtoolsModule } from "@ngrx/store-devtools";
@NgModule({
  declarations: [AppComponent, HeaderComponent, CityComponent, MapComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    FlexLayoutModule,
    MaterialModule,
    BrowserAnimationsModule,
    StoreModule.forFeature(fromData.DATA_FEATURE_KEY, fromData.reducer),
    StoreModule.forRoot({}), 
    EffectsModule.forFeature([DataEffects]),
    EffectsModule.forRoot([DataEffects]),
    StoreDevtoolsModule.instrument({maxAge: 25, logOnly: environment.production})
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
