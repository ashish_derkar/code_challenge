import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
@Component({
  selector: 'angular-nest-city',
  templateUrl: './city.component.html',
  styleUrls: ['./city.component.scss']
})
export class CityComponent  {

  city: any;
  constructor(
    private _mdr: MatDialogRef<CityComponent>,
    @Inject(MAT_DIALOG_DATA) data: string
  ) {
    console.log('data', data);
    this.city = data;
  }
  CloseDialog() {
    this._mdr.close(false)
  }
}
