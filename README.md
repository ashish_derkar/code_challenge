

# City App [Angular - NestJs - NgRx - Nx]

This project was generated using [Nx](https://nx.dev).

## How to run the application.

1. Take a clone of this repo - 

git clone https://ashish_derkar@bitbucket.org/ashish_derkar/code_challenge.git

2. Navigate to code_challenge directory -

cd code_challenge

3. Install node packages -

npm install

4. Run frontend application (open separate terminal in vscode or use Nx Console) -


 npx nx serve city-app --configuration=development   


5. Run backend application (open separate terminal in vscode or use Nx Console) -

 npx nx serve api


6. Navigate to http://localhost:4200/ in browser to see working application. 


## Build

Run `npx nx build city-app --configuration=production` to build the frontend project. The build artifacts will be stored in the `dist/city-app` directory.


Run `npx nx build api` to build the backend project. The build artifacts will be stored in the `dist/api` directory.



## Running unit tests

Run `ng test city-app` to execute the unit tests via [Jest](https://jestjs.io).


Run `nx affected:test` to execute the unit tests affected by a change.

## Running end-to-end tests

Run `ng e2e city-app` to execute the end-to-end tests via [Cypress](https://www.cypress.io).


Run `nx affected:e2e` to execute the end-to-end tests affected by a change.
